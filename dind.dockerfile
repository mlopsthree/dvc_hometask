FROM python:3.11-slim

RUN mkdir /app
WORKDIR /app

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.8.2

RUN apt-get update && apt-get install -y gcc && apt-get -y clean
RUN pip install --no-cache-dir --upgrade pip wheel

RUN pip install "poetry==$POETRY_VERSION"

# Копируем зависимости
COPY poetry.lock pyproject.toml /app/

# Установка зависимостей
RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi --with dev --no-root
