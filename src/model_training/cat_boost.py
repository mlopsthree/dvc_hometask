from catboost import CatBoostClassifier
from scipy.sparse import csr_matrix
import numpy as np


class CatboostTrainer():
    """
    A class for training CatBoost models using cross-validation.

    Methods
    -------
    split_data(X_train, y_train, num_parts)
        Splits the input data into multiple parts for cross-validation.

    train_cat_model(X_folds, y_folds, params, model_path)
        Trains a CatBoost model using the input data folds and model parameters.

    fit_catboost(X_train, y_train, params, num_parts)
        Fits a CatBoost model using the input data and parameters with cross-validation.
    """
    def split_data(self,
                   X_train: csr_matrix,
                   y_train: np.array,
                   num_parts: int) -> tuple[list[csr_matrix], list[np.array]]:
        """
        Splits the input data into multiple parts for cross-validation.

        Parameters
        ----------
        X_train : csr_matrix
            The input feature matrix.
        y_train : np.array
            The target labels.
        num_parts : int
            The number of parts to split the data into.

        Returns
        -------
        tuple[list[csr_matrix], list[np.array]]
            A tuple containing the split feature matrix and target labels.
        """

        X_train_split = []

        # Calculate the number of rows in each part
        num_rows_per_part = X_train.shape[0] // num_parts

        # Split the matrix into parts
        for i in range(num_parts):
            start_row = i * num_rows_per_part
            n_size = (i+1) * num_rows_per_part
            end_row = n_size if i < num_parts - 1 else X_train.shape[0]
            X_train_split.append(csr_matrix(X_train[start_row:end_row]))

        y_train_split = np.array_split(y_train, num_parts)

        # Check split sizes
        for i in range(0, num_parts):
            if not (y_train_split[i].shape[0] == X_train_split[i].shape[0]):
                raise Exception('Not equal split sizes')

        return X_train_split, y_train_split

    def train_cat_model(self,
                        X_folds: list[csr_matrix],
                        y_folds: list[np.array],
                        params: tuple,
                        model_path: str) -> CatBoostClassifier:
        """
        Trains a CatBoost model using the input data folds and model parameters.

        Parameters
        ----------
        X_folds : list[csr_matrix]
            The list of input feature matrices for each fold.
        y_folds : list[np.array]
            The list of target labels for each fold.
        params : tuple
            The parameters for the CatBoost model.
        model_path : str
            The path to save the trained model.

        Returns
        -------
        CatBoostClassifier
            The trained CatBoost model.
        """

        num_parts = len(X_folds)
        model = CatBoostClassifier(**params)

        for i in range(0, num_parts - 1):
            print(f'Start fitting fold #{i}')
            if i == 0:
                model.fit(X_folds[i], y_folds[i],
                          eval_set=(X_folds[9], y_folds[9]))
            else:
                model.fit(X_folds[i], y_folds[i],
                          eval_set=(X_folds[9], y_folds[9]),
                          init_model=model_path)
            model.save_model(model_path)

        return model

    def fit_catboost(self,
                     X_train: csr_matrix,
                     y_train: np.array,
                     params: tuple,
                     model_path: str,
                     num_parts: int = 10) -> CatBoostClassifier:
        X, y = self.split_data(X_train, y_train, num_parts)
        save_path = model_path + 'catboost.cbm'
        model = self.train_cat_model(X,
                                     y,
                                     params,
                                     save_path)
        return model
