import json
from pathlib import Path
import click
import dvc.api
from src.data_processing.utils import make_dir
from src.model_training.log_reg import train_log_reg
from src.model_training.cat_boost import CatboostTrainer
import numpy as np
import scipy as sp
from sklearn.metrics import ConfusionMatrixDisplay, classification_report

from matplotlib import pyplot as plt
from matplotlib.figure import Figure


def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(y_true, pred, ax=ax, colorbar=False)
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title("Confusion Matrix")
    plt.tight_layout()
    return fig


def test(model,
         data: np.ndarray,
         target: np.ndarray) -> tuple[dict, Figure]:
    predicts = model.predict(data)
    fig = conf_matrix(target, predicts)
    return classification_report(target, predicts, output_dict=True), fig


def eval_model(model,
               test_features,
               test_target,
               metric_path: Path,
               figure_path: Path) -> None:

    result, fig = test(model, test_features, test_target)
    json.dump(result, metric_path.open("w"))
    plt.savefig(figure_path)


@click.command()
@click.argument("model_path", type=str)
@click.argument("metric_path", type=Path)
@click.argument("figure_path", type=Path)
def train_and_eval(model_path: str,
                   metric_path,
                   figure_path) -> None:
    # load params
    model_params = dvc.api.params_show()
    vectorizer_name = model_params['model_and_vectorizer']['vectorizer']
    print(f'Chosen vectorizer: {vectorizer_name}')

    model_name = model_params['model_and_vectorizer']['model']
    print(f'Chosen model: {model_name}')

    # load target
    y_train = np.load("./data/final/y_train.npy")
    y_test = np.load("./data/final/y_test.npy")

    X_train = sp.sparse.load_npz(f"./data/final/{vectorizer_name}/x_train.npz")
    X_test = sp.sparse.load_npz(f"./data/final/{vectorizer_name}/x_test.npz")

    make_dir(model_path)
    if model_name == 'catboost':
        print(f'start training {model_name}')
        trainer = CatboostTrainer()
        model = trainer.fit_catboost(X_train,
                                     y_train,
                                     model_params['catboost'],
                                     model_path)

    elif model_name == 'logistic_regression':
        print(f'start training {model_name}')
        model = train_log_reg(X_train,
                              y_train,
                              model_params['logistic_regression'],
                              model_path)
    eval_model(model, X_test, y_test, metric_path, figure_path)


if __name__ == "__main__":
    train_and_eval()
