from sklearn.linear_model import LogisticRegression
import pickle
from pathlib import Path


def train_log_reg(x, y, params, model_path) -> LogisticRegression:
    model = LogisticRegression(**params)
    model.fit(x, y)
    model_name = Path(model_path + 'log_reg.pkl')
    pickle.dump(model, model_name.open("wb"))
    return model
