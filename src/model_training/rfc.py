import numpy as np
import scipy as sp
import time
import dvc.api
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report
from os import cpu_count


print("Load data")
"""Load target"""
y_train = np.load("../data/final/y_train.npy")
y_test = np.load("../data/final/y_test.npy")

"""Load features"""
X_train_bow = sp.sparse.load_npz("../data/final/bow_vectorizer/x_train.npz")
X_test_bow = sp.sparse.load_npz("../data/final/bow_vectorizer/x_test.npz")

X_train_tfidf = sp.sparse.load_npz("../data/final/tfidf_vectorizer/x_train.npz")
X_test_tfidf = sp.sparse.load_npz("../data/final/tfidf_vectorizer/x_test.npz")

'''Load params'''
model_params = dvc.api.params_show()

print('Start training')

start = time.time()
# Initialize the RandomForest classifier
rf_clf = RandomForestClassifier(n_estimators=10,
                                n_jobs=cpu_count() - 1,
                                random_state=42)

# Train the classifier
rf_clf.fit(X_train_bow, y_train)

end = time.time() - start
print(f"Done in {end}, evaluating ...")
y_pred_bow = rf_clf.predict(X_test_bow)

# Evaluate the classifier
accuracy_bow = accuracy_score(y_test, y_pred_bow)
print("BoW Accuracy:", accuracy_bow)
print("Classification Report with BoW:")
print(classification_report(y_test, y_pred_bow))
