# Preprocessing function
import nltk
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
import re
nltk.download('punkt')
nltk.download('stopwords')


def preprocess_text(text: str) -> str:
    """
    Preprocess the given text by performing the following operations:
    1. Convert the text to lowercase
    2. Tokenize the text into individual words
    3. Remove punctuation from the tokens
    4. Remove stopwords from the tokens
    5. Stem the remaining tokens using Porter Stemmer
    6. Join the stemmed tokens back into a single string

    Args:
        text (str): The input text to be preprocessed.

    Returns:
        str: The preprocessed text.
    """
    text = str(text)
    text = text.lower()
    tokens = nltk.word_tokenize(text)
    # Removing punctuation
    tokens = [token for token in tokens if re.match(r'^\w+$', token)]
    # Removing stopwords
    stop_words = set(stopwords.words('english'))
    tokens = [token for token in tokens if token not in stop_words]
    # Stemming
    stemmer = PorterStemmer()
    tokens = [stemmer.stem(token) for token in tokens]
    return ' '.join(tokens)
