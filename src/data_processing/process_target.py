import pandas as pd
import numpy as np
import click

from utils import make_dir


def convert_y(y_path,
              file_name):
    y = pd.read_parquet(y_path)
    np.save(file_name,
            y.iloc[:, 0].to_numpy())


@click.command()
@click.argument("input_path", type=str)
@click.argument("output_path", type=str)
def process_target(input_path: str,
                   output_path: str) -> None:
    make_dir(str(output_path))
    convert_y(input_path + 'y_train.parquet',
              output_path + 'y_train.npy')
    convert_y(input_path + 'y_test.parquet',
              output_path + 'y_test.npy')


if __name__ == "__main__":
    process_target()
