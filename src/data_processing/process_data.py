import pandas as pd
import click
from pathlib import Path
import time

from text_processor import preprocess_text
from utils import make_dir


def load_dataset(dataset_path: str) -> pd.DataFrame:
    """
    Load a dataset from a CSV file.

    Args:
        dataset_path (str): The path to the CSV file containing the dataset.

    Returns:
        pd.DataFrame: A pandas DataFrame containing the loaded dataset.
    """
    df = pd.read_csv(dataset_path,
                     header=None,
                     names=("polarity", "title", "text")
                     )
    return df


def process_dataframe(df: pd.DataFrame) -> pd.DataFrame:
    """
    Preprocess the text data in a pandas DataFrame.

    This function concatenates the 'title' and 'text' columns of the input DataFrame,
    applies the preprocess_text function from the text_processor module to the
    concatenated text, and stores the preprocessed text in a new 'clean_text' column.

    Args:
        df (pd.DataFrame): The input DataFrame containing 'title' and 'text' columns.
    Returns:
        pd.DataFrame: The input DataFrame with a new 'clean_text' column containing
                      the preprocessed text data.
    """
    pd.options.mode.chained_assignment = None  # remove warning
    df["clean_text"] = df["title"] + " " + df["text"]
    df["clean_text"] = df["clean_text"].apply(preprocess_text)
    return df


def process_and_save(input_path: str,
                     output_path: str) -> None:
    """
    Load a dataset from a CSV file, preprocess the text data,
    and save the processed data as Parquet files.

    This function loads a dataset from a CSV file specified by `input_path`,
    preprocesses the text data  by calling the `process_dataframe` function,
    and saves the preprocessed text and the corresponding polarity labels
    as separate Parquet files in the directory specified by `output_path`.
    The preprocessed text is saved with a filename prefix of 'x_'
    followed by the original filename, and the polarity labels
    are saved with a filename prefix of 'y_' followed by the original filename.

    Args:
        input_path (str): The path to the CSV file containing the dataset.
        output_path (str): The path to the directory where
                           the processed data should be saved.

    Returns:
        None
    """
    df = load_dataset(input_path)
    print(f"start processing {input_path.split('/')[-1]}")
    start = time.time()
    df_processed = process_dataframe(df)
    df_name = input_path.split('/')[-1].split('.')[0]
    end = time.time() - start
    print(f"{df_name} data processed in {end} , saving...")

    x = df_processed["clean_text"].to_frame()
    x.to_parquet(output_path + f'/x_{df_name}.parquet',
                 engine='fastparquet',
                 index=False)

    y = df_processed["polarity"].to_frame()
    y.to_parquet(output_path + f'/y_{df_name}.parquet',
                 engine='fastparquet',
                 index=False)


@click.command()
@click.argument("input_path", type=Path)
@click.argument("output_path", type=Path)
def preprocess_data(input_path: Path,
                    output_path: Path) -> None:
    make_dir(str(output_path))
    process_and_save(str(input_path), str(output_path))


if __name__ == "__main__":
    preprocess_data()
