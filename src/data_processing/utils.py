import os


def make_dir(path: str) -> None:
    """
    Create a directory at the specified path if it doesn't already exist.

    Args:
        path (str): The path where the directory should be created.

    Returns:
        None
    """
    if os.path.exists(path):
        pass
    else:
        os.mkdir(path)
