import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
import dvc.api


def train_bow(X_train: pd.DataFrame) -> CountVectorizer:
    """
    Train a Bag-of-Words (BoW) vectorizer on the training data.

    Args:
        X_train (pd.DataFrame): A DataFrame containing the text data
        to be used for training.

    Returns:
        CountVectorizer: A fitted CountVectorizer object that can be used
        to transform text data into a BoW representation.
    """
    params = dvc.api.params_show()
    bow_vectorizer = CountVectorizer(**params["bow_vectorizer"])
    bow_vectorizer.fit(X_train.clean_text)
    return bow_vectorizer
