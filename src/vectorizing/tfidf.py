import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import dvc.api


def train_tfidf(X_train: pd.DataFrame) -> TfidfVectorizer:
    """
    Train a Term Frequency-Inverse Document Frequency (TF-IDF)
    vectorizer on the training data.

    Args:
        X_train (pd.DataFrame): A DataFrame containing the text data
        to be used for training.

    Returns:
        TfidfVectorizer: A fitted TfidfVectorizer object that can be used
        to transform text data into a TF-IDF representation.
    """
    params = dvc.api.params_show()
    tfidf_vectorizer = TfidfVectorizer(**params["tfidf_vectorizer"])
    tfidf_vectorizer.fit(X_train.clean_text)
    return tfidf_vectorizer
