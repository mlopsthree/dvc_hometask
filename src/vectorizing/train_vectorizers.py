import pandas as pd
import click
import pickle
from pathlib import Path

from src.data_processing.utils import make_dir
from src.vectorizing.bag_of_worlds import train_bow
from src.vectorizing.tfidf import train_tfidf


@click.command()
@click.argument("x_train_path", type=str)
@click.argument("vectorizers_path", type=str)
def train_vectorizers(x_train_path: str,
                      vectorizers_path: str) -> None:

    make_dir(vectorizers_path)
    X_train = pd.read_parquet(x_train_path)
    bow_vectorizer = train_bow(X_train)
    bow_path = Path(f'{vectorizers_path}bow_vectorizer.pkl')
    pickle.dump(bow_vectorizer, bow_path.open("wb"))
    tfidf_vectorizer = train_tfidf(X_train)
    tfidf_path = Path(f'{vectorizers_path}tfidf_vectorizer.pkl')
    pickle.dump(tfidf_vectorizer, tfidf_path.open("wb"))


if __name__ == "__main__":
    train_vectorizers()
