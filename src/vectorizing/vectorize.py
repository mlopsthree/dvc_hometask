import pandas as pd
import click
import scipy as sp
import pickle
from pathlib import Path

from src.data_processing.utils import make_dir


def vectorize(vectorizer, data: pd.DataFrame) -> pd.DataFrame:
    """
    Transform text data into a vectorized representation using a given vectorizer.

    Args:
        vectorizer (CountVectorizer or TfidfVectorizer): A fitted vectorizer object
            that can be used to transform text data into a vector representation.
        data (pd.DataFrame): A DataFrame containing the text data to be vectorized.

    Returns:
        pd.DataFrame: A DataFrame containing the vectorized representation of the
            input text data.
    """
    return vectorizer.transform(data)


@click.command()
@click.argument("x_train_path", type=str)
@click.argument("x_test_path", type=str)
@click.argument("model_path", type=Path)
@click.argument("final_path", type=str)
def vectorize_data(x_train_path: str,
                   x_test_path: str,
                   model_path: Path,
                   final_path: str):

    X_train = pd.read_parquet(x_train_path).clean_text
    X_test = pd.read_parquet(x_test_path).clean_text
    model = pickle.load(model_path.open("rb"))
    result_train = model.transform(X_train)
    result_test = model.transform(X_test)
    make_dir(final_path)
    sp.sparse.save_npz(final_path + 'x_train.npz', result_train)
    sp.sparse.save_npz(final_path + 'x_test.npz', result_test)


if __name__ == "__main__":
    vectorize_data()
